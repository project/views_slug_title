INTRODUCTION
------------
With Views slug title, you will easily be able to use the title of your node 
as arguments. This module includes a field similar to "URL path settings" 
except that views slug title generates only the title.

The character will be replaced or deleted based on the settings configured 
in "alias url". In views, a contextual filter widget is available and allows 
you to associate for you, automatically, the "slug" to the node. It is also 
available as a single field in order to re-write an url, for example, 
in a node list. The field is available only for nodes and does not yet 
work for other entities.

 * For a full description of the module, visit the project page:
   https://drupal.org/sandbox/niknak/2418497



REQUIREMENTS
------------
This module requires the following modules:

 * Views (https://drupal.org/project/views)
 * Pathauto (https://drupal.org/project/pathauto)
 * Token (https://drupal.org/project/token)



INSTALLATION
------------
 * Install as you would normally install a contributed Drupal module. See:
   https://drupal.org/documentation/install/modules-themes/modules-7
   for further information.
